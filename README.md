# Wordpress Plugin for FOSSASIA Api

The goal of the wordpress plugin for the FOSSASIA API is to enable communities list themselves in the network and provide a starting point for interested contributors, who are searching for Open Source communitiies nearby.

The FOSSASIA Api is based on the Freifunk Api and the Hackerspaces API (http://hackerspaces.nl/spaceapi/). Each community provides its data in a well defined format, hosted on their places (web space, wiki, web servers) and contributes a link to the directory. This directory only consists of the name and an url per community. First services supported by our freifunk API are the global community map and a community feed aggregator. The FOSSASIA API is designed to collect metadata of communities in a decentralised way and make it available to other users.

## Communication

Please join our mailing list to discuss questions regarding the project: https://groups.google.com/forum/#!forum/fossasia

Our chat channels are on gitter here: https://gitter.im/fossasia/
[![Join the chat at https://gitter.im/fossasia/api.fossasia.net](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/fossasia/api.fossasia.net?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

## Toolbox

Set of utility scripts to process, aggregate, and extract information from FOSSASIA communities data

### Components

* API data collector & aggregator [collector/collectCommunities.py](https://github.com/fossasia/common.api.fossasia.net/blob/master/collector/collectCommunities.py)

* Set of tools to work with `.ics` format, including an ics collector, parser, merger and debugger ([live demo](http://api.fossasia.net/ics-collector/debugger/)).

* FOSSASIA Calendar API. Check out the details in [API wiki](https://github.com/fossasia/common.api.fossasia.net/blob/master/ics-collector/README.md)

* and more

[FOSSASIA API repo](https://github.com/fossasia/api.fossasia.net)

## Requirements

* `directory.json` (collector/collectCommunities.py)
* `ffSummarizedDir.json` (ics-collector/ics-collector.php)
* Software version :
  * PHP : >= 5.4
  * Python : >= 3.4


## API
The API is designed to integrate independent components. Only harmless urls hold them together, which can, in most of the case, be modified in project's configuration file. Please clone any repo as per your needs and refer to its README file for more information.

## Functionality

The "back-end" of FOSSASIA API project is a set communities json files, each containing some general meta-information about a community: name, location, contact, blog/forum/calendar feeds.. Just enough to build interesting projects and statistics. API files must follow a well-defined format, and to make things easier, there's a web-based file generator to help creating / modifying them. Format specification and API file generator are situated at the current [api](https://github.com/fossasia/api.fossasia.net) repo.

From the list of [individual communities files urls](https://github.com/fossasia/directory.api.fossasia.net/blob/master/directory.json), we retrieve API json files and cache them in aggregated files : `ffSummarizedDir.json`, `ffGeoJson.json`, `ffMerged.ics`,using the common aggregator & ics merger in [common](https://github.com/fossasia/common.api.fossasia.net) repo. Other API projects & external services can retrieve API "back-end" data from these files. These aggregator are set up as cron tasks to periodically retrieve fresh data.

The API is designed to make communities metadata available to everyone. There are several public webservice endpoints that users can access : Calendar API in [common](https://github.com/fossasia/common.api.fossasia.net) repo, merged feed rss in [feed](https://github.com/fossasia/feed.api.fossasia.net) repo. These services require the aggregated json file `ffGeoJson.json`.

Finally, there are several visual components: the common map situated at [cmap](https://github.com/fossasia/cmap.api.fossasia.net) repo, that could be embed as iframe in any website. The map also requires `ffGeoJson.json`. And the community timeline in [timeline](https://github.com/fossasia/timeline.api.fossasia.net) repo, which gets its data from Calendar API, and is bundled as a jQuery plugin